﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsNamespace
{
    class Abs_Equation : Equation
    {
        public float A
        {
            get;
            set;
        }
        public override float GetValueInPoint(float X)
        {
            return (Math.Abs(A * X) + base.OffsetY);
        }
        public override string ToString()
        {
            return "Y = " + "|" + (A >= 0 ? " " : "") + A + "*" + "X" + "|" + (base.OffsetY >= 0 ? " + " : "") + base.OffsetY;
        }
        public Abs_Equation(float A, float OffsetY, float FromX, float ToX)
        {
            this.A = A;
            base.OffsetY = OffsetY;
            base.FromX = FromX;
            base.ToX = ToX;
        }
    }
}
