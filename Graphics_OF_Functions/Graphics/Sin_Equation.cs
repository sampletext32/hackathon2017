﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsNamespace
{
    class Sin_Equation : Equation
    {
        public float A
        {
            get;
            set;
        }
        public float B
        {
            get;
            set;
        }
        public override float GetValueInPoint(float X)
        {
            return A*(float)Math.Sin(B*X) + OffsetY;
        }
        public override string ToString()
        {
            return "Y = " + (A >= 0 ? " " : "") + A + "*" + "sin" + "(" + (B >= 0 ? " " : "") + B + "X" +")" + (base.OffsetY >= 0 ? " + " : " - ") + base.OffsetY;
        }
        public Sin_Equation(float A,float B, float OffsetY, float FromX, float ToX)
        {
            this.A = A;
            this.B = B;
            base.OffsetY = OffsetY;
            base.FromX = FromX;
            base.ToX = ToX;
        }
    }
}
