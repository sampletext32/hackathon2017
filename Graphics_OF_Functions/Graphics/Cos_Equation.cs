﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphicsNamespace
{
    class Cos_Equation : Equation
    {
        public float A
        {
            get;
            set;
        }
        public float B
        {
            get;
            set;
        }
        public override float GetValueInPoint(float X)
        {
            return A*(float)Math.Cos(B*X) + OffsetY;
        }
        public override string ToString()
        {
            return "Y = "+ (A >= 0 ? " " : "") + A +"*"+"cos"+"("+ (B >= 0 ? " " : "") + B + "X" + ")" + (base.OffsetY >= 0 ? " + " : " - ") + base.OffsetY;
        }
        public Cos_Equation(float A, float B, float OffsetY, float FromX, float ToX)
        {
            this.A = A;
            this.B = B;
            base.OffsetY = OffsetY;
            base.FromX = FromX;
            base.ToX = ToX;
        }
    }
}
