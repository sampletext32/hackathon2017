﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman
{
    class PacmanController
    {
        public Cell[,] field = new Cell[10, 10];

        public PacmanController()
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == 5)
                    {
                        field[i, j] = new Cell(CellType.Wall);
                    }
                    else field[i, j] = new Cell(CellType.MovePlace);
                }
            }
        }
    }
}
