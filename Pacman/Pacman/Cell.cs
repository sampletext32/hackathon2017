﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pacman
{
    enum CellType
    {
        Pacman,
        Wall,
        WithDot,
        MovePlace,
        Ghost,
        Energizer
    }
    class Cell
    {
        public CellType cellType;
        public Cell(CellType type)
        {
            cellType = type;
        }
    }
}
