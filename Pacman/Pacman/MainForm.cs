﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pacman
{
    public partial class MainForm : Form
    {
        PacmanController Controller;
        int CellSize = 80;
        public MainForm()
        {
            InitializeComponent();
            Controller = new PacmanController();
        }

        private void pictureBoxGameField_Paint(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (Controller.field[i, j].cellType == CellType.Wall)
                    {
                        e.Graphics.FillRectangle(Brushes.DarkBlue, i * CellSize, j * CellSize, CellSize, CellSize);
                    }
                    if (Controller.field[i, j].cellType == CellType.MovePlace)
                    {
                        e.Graphics.FillRectangle(Brushes.LightBlue, i * CellSize, j * CellSize, CellSize, CellSize);
                        e.Graphics.DrawImage(Properties.Resources.lehman, i * CellSize, j * CellSize, CellSize, CellSize);
                    }
                    e.Graphics.DrawRectangle(Pens.Black, i * CellSize, j * CellSize, CellSize, CellSize);
                    
                }
            }
        }
    }
}
